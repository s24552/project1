#include <iostream>
#include <random>

auto myFunction(int const x) -> void
{
    std::cout << x << std::endl;
}

auto call_with_random_int (void(*fp)(int const)) -> void
{
    std::random_device device;
    std::uniform_int_distribution<int> d100(1,100);
    auto x = d100(device);
    (*fp)(x);
}


