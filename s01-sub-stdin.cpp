#include <iostream>
#include <string>

   auto ask_for_int(std::string hint) -> int

    {
    std::cout << hint << "(write an integer)";
    auto a = std::string{};
    std::getline(std::cin, a);
    return std::stoi(a);
    }

    auto main() ->int
     {
    auto const first = ask_for_int("first:");
    auto const second = ask_for_int("second:");
    std::cout << "the result is ";
    std::cout <<(first - second) << "\n";
    return 0;

    }
