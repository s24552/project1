#include <iostream>
#include <string>

auto main (int argc, char* argv[]) -> int
{
    auto password = std::string{argv[1]};
    auto password_try = std::string{};
    do{
        std::cout << "password: ";
        std::getline (std::cin, password_try);


        } while (password_try != password);


    std::cout << "thank you, the password is correct \n";
    return 0;
}
