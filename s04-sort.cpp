#include <iostream>

 int myFunction (void* a, void*b)
 {
    if ( *(int*)a <  *(int*)b )
    {
        return -1;
    }
    if ( *(int*)a == *(int*)b )
    {
        return 0;
    }
    if ( *(int*)a >  *(int*)b )
    {
        return 1;
    }
 }
auto fpsort(void* a[], size_t n, int (*fp)(void*, void*)) -> void
{
    int tmp;

    for(int i=0; i<n; i++)
    {
        int j=i;
        for(int h=i; h<n; h++)
        {
            if(fp(&a[j], &a[h]))
            {
                j=h;
            }
        }
        tmp =*(static_cast<int*>(a[i]));
        *(static_cast<int*>(a[i])) = *(static_cast<int*>(a[j]));
        *(static_cast<int*>(a[j])) = tmp;
    }
        for(int i=0; i<n; i++)
    {
        std::cout << *(int*)a[i];
    }
}


