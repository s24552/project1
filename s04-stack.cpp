#include <iostream>
#include <string>
#include <stack>

auto main(int argc, char* argv[]) -> int
{
    auto argument = std::string{argv[1]};
    auto stackArg = std::stack<char>{};
    stackArg.push(argument[0]);
    for(int i=1; i<argument.size(); i++)
    {
        if(argument[i]=='(' || argument[i] =='{' || argument[i] =='[')
        {
            stackArg.push(argument[i]);
        }
        else if(argument[i]==')')
        {
            if(stackArg.top() == '(')
            {
                 stackArg.pop();
            }
            else
            {
                break;
            }
        }
        else if(argument[i]=='}')
        {
            if(stackArg.top() == '{')
            {
                 stackArg.pop();
            }
            else
            {
                break;
            }
        }
        else if(argument[i]==']')
        {
            if(stackArg.top() == '[')
            {
                 stackArg.pop();
            }
            else
            {
                break;
            }
        }
    }
    stackArg.empty() ? std::cout << "OK\n" : std::cout << "ERROR\n";
    
    return 0;
}
