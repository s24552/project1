#include <iostream>
#include <string>


auto getch() ->char
{
    system("stty raw");
    auto input = getchar();
    system("stty cooked");
    return input;
}

auto main(int argc, char* argv[]) ->int
{   auto width =10;
    auto hight = 10;
    auto x=1;
    auto y=1;
    char input;
    std::string normal = "\033[0;39m";
    std::string green = "\033[0;32m";
    if (argc >1)
    {
        width = std::stoi(argv[1]);
        hight = std::stoi(argv[2]);
    }
    while(input !='q')
    {   
        system("clear"); 
        for(int i=0; i<hight+2; i++)
        {
         for(int j=0; j<width+2; j++)
            {
                if(i==0 || i == hight+1 || j==0 || j == width+1) 
                {
                    std::cout << "#";
                }
                else if(i==y && j==x) 
                {
                    std::cout << green << "*" << normal;
                }
                else 
                {
                    std::cout << " ";
                }
            }
            std::cout<< "\n";
        }   
        std::cout<<"\n";
        input=getch();
        switch(input)
        {
        case 'w':
            y--;
            break;
        case 's':
            y++;
            break;
        case 'a':
            x--;
            break;
        case 'd':
            x++;
            break;
        }
        if(x<1)
            x=1;
        if(x>width)
            x=width;
        if(y<1)
            y=1;
        if(y>hight)
            y=hight;
    }
    return 0;
}
