#include <iostream>

int main()
{
int bottles = 99;
while(bottles > 2)
    {
    std::cout << bottles << " bottles of beer on the wall, " << bottles << " bottles of beer.\n"; 
    std::cout << "Take one down, pass it around, " << --bottles << " bottles of beer on the wall...\n";         
    }
std::cout << "2 bottles of beer on the wall, 2 bottles of beer.\n";
std::cout << "Take one down, pass it around, 1 bottle of beer on the wall...\n";
std::cout << "1 bottle of beer on the wall, 1 bottle of beer.\n";
std::cout << "Take one down, pass it around, No more bottles of beer on the wall...\n";
std::cout << "No more bottles of beer on the wall, no more bottles of beer.\n";
std::cout << "Go to the store and buy some more, 99 bottles of beer on the wall...\n";
return 0;
}
