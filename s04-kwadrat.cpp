#include <iostream>
#include <stdexcept>
#include <vector>

struct Rectangle
{
    float hight;
    float width;
    
    Rectangle(float x, float y): hight(x), width(y)
    {
        if(hight<0 || width<0)
        {
            throw std::logic_error{"only positive values are allowed"};
        }
    }
    auto area() const -> float
    {
        return hight * width;
    }
    auto draw() const -> void
    {
        for(int i=0; i < hight; i++)
        {
            for(int j=0; j < width; j++)
            {
                std::cout << "*";
            }
            std::cout << std::endl;
        }
    }
    auto scale(float const x, float const y) -> void
    {
        if(x<0 || y<0)
        {
            throw std::logic_error{"only positive values are allowed"};
        }
        hight *= x;
        width *= y;
    }
};

auto main(int argc, char* argv[]) -> int
{
    try
    {
        auto x = std::stof (argv[1]);
        auto y = std::stof (argv[2]);
        auto const scale_x = std::stof (argv[3]);
        auto const scale_y = std::stof (argv[4]);
        Rectangle myRect(x,y);
        myRect.scale(scale_x, scale_y);
        myRect.draw();
    }
    catch(std::logic_error const& e)
    {
    std::cerr << "error: " << e.what() << std::endl;
    }
    return 0;
}


