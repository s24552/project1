#include <iostream>
#include <string>


auto main(int argc, char* argv[]) ->int
{
    auto tmp_a = std::string{argv[1]};
    auto tmp_b = std::string{argv[2]};

    auto  a = std::stof(tmp_a);
    auto  b = std::stof(tmp_b);

    if (b==0){
        std::cout << "you can't divide by 0!" << "\n";

    }else{
         std::cout << (a/b) << "\n";
    }
     return 0;
}
