#include <iostream>
#include <string>

auto main(int argc, char* argv[]) -> int
{
    auto  number = std :: stoi ( argv [1]);
    if(number>=0){

        while (number>=0) {
            std::cout << number << "...\n";
            number--;}
    }else{
            while (number<=0){
            std::cout << number << "...\n";
            number++;}
    }
    return 0;
}
