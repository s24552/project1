#include <iostream>
#include <string>

   auto ask_for_int(std::string hint) -> float

    {
    std::cout << hint << "(write an integer)";
    auto a = std::string{};
    std::getline(std::cin, a);
    return std::stof(a);
    }

    auto main() ->int
     {
    std::cout << "hello, let's count a little \n";
    auto const first = ask_for_int("first:");
    auto const second = ask_for_int("second:");
    if (second == 0){
    std::cout <<"you can't divide by 0 \n" ;
    } else {
    std::cout << "the result is ";
    std::cout <<(first / second) << "\n";}
    return 0;
     }
