#include <iostream>
#include <cstdio>

class MovedCursor
{
    int x;
    int y;
    char cursor;

    public:
    MovedCursor(): x(1), y(1), cursor('*')
    {
        std::cout << "\033[8;50;100t" << std::endl;
        system("clear");
        std::cout << "\033[" << y << ";" << x << "H";
        std::cout << cursor; 
    }
  
    auto MoveUp() -> void
    {
        std::cout << "\033[2J";
        y-=1;
        if (y<1)
        {
            y=1;
        }
        std::cout << "\033[" << y << ";" << x << "H";
        std::cout << cursor;        
    }

    auto MoveDown() -> void
    {
        std::cout << "\033[2J";
        y+=1;
        if (y>50)
        {
            y=50;
        }
        std::cout << "\033[" << y << ";" << x << "H";
        std::cout << cursor;        
    }

    auto MoveLeft() -> void
    {
        std::cout << "\033[2J";
        x-=1;
        if (x<1)
        {
            x=1;
        }
        std::cout << "\033[" << y << ";" << x << "H";
        std::cout << cursor;        
    }

    auto MoveRight() -> void
    {
        std::cout << "\033[2J";
        x+=1;
        if (x>99)
        {
            x=99;
        }
        std::cout << "\033[" << y << ";" << x << "H";
        std::cout << cursor;        
    }

};

auto main() -> int
{
    system("stty raw -echo");
    char pressedButton;
    auto stopButton = 'q';
    MovedCursor asterisk;
    while(pressedButton != stopButton)
    {
        pressedButton = getchar();
        if(pressedButton == 'w')
        {
            asterisk.MoveUp();
        }
        else if (pressedButton == 's')
        {
            asterisk.MoveDown();
        }
        else if (pressedButton == 'a')
        {
            asterisk.MoveLeft();
        }
        else if (pressedButton == 'd')
        {
            asterisk.MoveRight();
        }
    }
    system("stty cooked echo");
    return 0;
}

