#include <iostream>
#include <string>

   auto ask_for_int(std::string prompt) -> int

    {
    std::cout << prompt << "(write an integer)";
    auto a = std::string{};
    std::getline(std::cin, a);
    return std::stoi(a);
    }

    auto main() ->int
     {
    auto const first = ask_for_int("first:");
    auto const second = ask_for_int("second:");
    std::cout <<(first + second) << "\n";
    return 0;

    }
