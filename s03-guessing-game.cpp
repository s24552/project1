#include <iostream>
#include <random>
#include <string>

int main()
{
    std::random_device device;
    std::uniform_int_distribution<int> d100(1,100);
    auto x = d100(device);
    int guess;
    std::cout << "Guess the number from 1 to 100 \n";
    do 
    {
        std::cout <<"your guess: ";
        auto temp = std:: string {};
        std::getline(std::cin, temp);
        guess = std::stoi(temp);
        if(guess < x)
        {std::cout << "number is too small! \n";}
        else{ if(guess > x)
        {std::cout << "number is too big! \n";}
        else {std::cout << "just right! \n";}}
    } while(guess != x);
    return 0;
}
    

